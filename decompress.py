import sys, struct

def translate_header(header):
	mapping = {}
	for i in range(0, len(header), 2):
		character = chr(header[i])
		order_number = header[i+1]
		mapping[order_number] = character
	return mapping

infile_name = sys.argv[1]
outfile_name = sys.argv[2]

infile = open(sys.argv[1] , "rb")
header_length_binary = (infile.read(2))
header_length = struct.unpack("H", header_length_binary)[0]
header_binary = infile.read(header_length)
header = struct.unpack("%sB" % header_length, header_binary)

decompress_key = translate_header(header)
alphabet_size = (header_length / 2) + 1

compressed_text = infile.read()
num_longs = len(compressed_text) / 8
ints = struct.unpack("%sQ" % num_longs, compressed_text)


outfile = open(sys.argv[2], "wb")

for current in ints:
	currchunk = []
	while current != 0:
		letter_code = current % alphabet_size
		current /= alphabet_size
		currchunk.append(decompress_key[letter_code])
	currchunk = currchunk[::-1]
	for byte in currchunk:
		outfile.write(byte)
