import sys, struct

def count_characters(open_file):
	char_count = {}

	ch = open_file.read(1)
	while ch != "":
		if ch not in char_count.keys():
			char_count[ch] = 0
		char_count[ch] += 1
		ch = open_file.read(1)
	return char_count

# Sorts the keys of a dictionary based on the value they map to
# returns the keys in an array
def sort_keys(char_count):	
	arr = char_count.keys()
	arr = sorted(arr, key=lambda x: char_count[x])[::-1]
	return arr

def bind_numbers(sorted_arr):
	bindings = {}
	number = 1
	for ch in sorted_arr:
		bindings[ch] = number
		number += 1
	return bindings

# Maps the characters of a file to the order number they correspond
# to
def map_chars_to_num(open_file, bindings):
	char_nums = []
	for line in open_file:
		for ch in line:
			char_nums.append(bindings[ch])
	return char_nums

# the variable "base" is a bit of a misnomer, seeing that the numbers
# are technically of base "base + 1"
# For now, just uses the longest contiguous subarray to join numbers,
# but there is probably a more effective way to do this
def join_numbers(numbers, base):
	final_array = []
	# Upper bound ensures we don't make a number larger than 8 bytes
	UPPER_BOUND = 4294967296 ** 2
	current_number = 0
	for i in range(0, len(numbers)):
		current_number = current_number * base + numbers[i]
		# Check if the next number will put us over the upper bound.
		# If it does, we want to add the current number to the array and reset
		# the current number
		if i + 1 < len(numbers):
			next_number = current_number * base + numbers[i + 1]
		else:
		# If we are on the last number, we want to save the current number
		# to the final array regardless of the current number's value
			next_number = UPPER_BOUND
		if next_number >= UPPER_BOUND:
			final_array.append(current_number)
			current_number = 0
	return final_array

def get_translate_key(mapping):
	translate_key = []
	# put the header length as the first 2 bytes
	translate_key.append(len(mapping.keys()) * 2)
	# Put the character first, then the order number
	for key in mapping.keys():
		translate_key.append(ord(key))
		translate_key.append(mapping[key])
	return translate_key


solution = count_characters(open(sys.argv[1], "rb"))
arr = sort_keys(solution)
alphabet_size = len(arr)

bindings = bind_numbers(arr)

# number_char_array is the mapping of every character in the file to its
# corresponding order number
number_char_array = map_chars_to_num(open(sys.argv[1]), bindings)
num_compress_array = join_numbers(number_char_array, alphabet_size + 1)
for x in num_compress_array[-4:-1]:
	print x
	print len(str(x))

num_items = "%dQ" % len(num_compress_array)
header_size = "H%dB" % (2 * alphabet_size)

translate_key = get_translate_key(bindings)

outfile = open(sys.argv[1] + ".cpr", "wb")
outfile.write(struct.pack( header_size, *translate_key) )
outfile.write(struct.pack( num_items, *(num_compress_array)))
outfile.close()
